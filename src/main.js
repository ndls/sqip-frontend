import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

/* Use Event.$emit / Event.$on for event handler */
window.Event = new Vue()


new Vue({
  render: h => h(App),
}).$mount('#app')
